;;; closing-brace-annotation-mode.el --- Mode for showing where a closing brace is -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Sam Precious
;;
;; Author: Sam Precious <samwdp@gmail.com>
;; Maintainer: Sam Precious <samwdp@gmail.com>
;; Created: March 17, 2022
;; Modified: March 17, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/samwd/closing-brace-annotation-mode
;; Package-Requires: ((emacs "26.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Mode for showing where a closing brace is
;;
;;; Code:
(defcustom closing-brace-annotation-enable t
  "Whether or not to enable ‘closing-brace-annotation’."
  :type 'boolean
  :group 'closing-brace-annotation)

(defvar-local closing-brace-annotation--ovs nil
  "Overlays used by `closing-brace-annotation'.")

(defvar ml-text-scale-factor 1.0
  "Scale of mode-line font size to default font size, as a float.
This is needed to make sure that text is properly aligned.")

(defun closing-brace-annotation--delete-ov()
  "Removes all the annotations"
 (seq-do 'delete-overlay closing-brace-annotation--ovs)
 (setq closing-brace-annotation--ovs nil))

(defun closing-brace-annotation--populate-ov ()
  "populate the overlay with text"
  (let* ((ov (make-overlay (point-at-eol) -1 nil t t))
         (text-value " <- hello"))
      (overlay-put ov 'after-string text-value)
      (overlay-put ov 'evaporate t)
      (push ov closing-brace-annotation--ovs)))


(defun closing-brace-annotation ()
  "Creates overlay for closing braces"
  (closing-brace-annotation--populate-ov))

(define-minor-mode closing-brace-annotation-mode
  "Minor mode for showing information for current line."
  :init-value nil
  (cond (closing-brace-annotation-mode
         (add-hook 'post-command-hook 'closing-brace-annotation nil t))
    (t
     (closing-brace-annotation--delete-ov)
     (remove-hook 'post-command-hook 'closing-brace-annotation t))))

(defun closing-brace-annotation-enable (enable)
  "Enable/disable `closing-brace-annotation-mode'."
  (closing-brace-annotation-mode (if enable 1 -1))
  (if enable
      (add-hook 'before-revert-hook 'closing-brace-annotation--delete-ov nil t)
    (remove-hook 'before-revert-hook 'closing-brace-annotation--delete-ov t)))

(provide 'closing-brace-annotation-mode)
;;; closing-brace-annotation-mode.el ends here
